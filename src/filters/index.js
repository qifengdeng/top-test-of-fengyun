import Vue from 'vue'
// 引入moment 时间模块 需要yarn add
import moment from 'moment'
Vue.filter('datafmt', (arg) => {
  return moment(arg).format('YYYY-MM-DD HH:mm:ss')
})
Vue.filter('datefmt', (arg) => {
  return moment(arg).format('MM-DD')
})
