import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/vant.js'
import './api'
import './filters'
import './mixins'
// 进度条样式
import 'nprogress/nprogress.css'
// 淘宝适配
// import 'lib-flexible'
Vue.config.productionTip = false
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
