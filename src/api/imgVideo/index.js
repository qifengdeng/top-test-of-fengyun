import axios from 'axios'
import Vue from 'vue'
export default Vue.prototype.$http = {
  async getSong() {
    return await axios.get('/getsong')
  }
}
