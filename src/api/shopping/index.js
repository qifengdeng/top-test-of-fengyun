import axios from 'axios'
const headers = {
  headers: {
    Authorization: localStorage.token
  }
}
export default {
  // 1.获取全部时装
  async getShiZhuang(params) {
    return await axios.get('/shizhuang?limit='+ params)
  },
  // 2.获取稀世时装
  async getXiShi(params) {
    return await axios.get('/xishi', params)
  },
  // 3.获取奇珍时装
  async getQiZhen(params) {
    return await axios.get('/qizhen', params)
  },
  // 4.获取求生者时装
  async getQiuSheng(params) {
    return await axios.get('/qiusheng', params)
  },
  // 5.获取监管者时装
  async getJianGuan(params) {
    return await axios.get('/jianguan', params)
  },
  // 6.根据时装id获取时装详情
  async getDetail(id) {
    return await axios.get('/shizhuangById?id=' + id)
  },
  // 7.根据用户id获取购物车列表
  async getCart(id) {
    return await axios.get('/getCart?id=' + id,headers)
  },
  // 8.将所有切换成未被选择
  async getCartAllNo(obj) {
    return await axios.put('/checkAllNo?', obj)
  },
  // 9.获取被选择的时装总价
  async getCartTotal(id) {
    return await axios.get('/totalPrice?uId=' + id)
  },
  // 10.加入购物车
  async addCart(obj) {
    return await axios.post('/addCart', obj,headers)
  },
  // 11.删除购物车时装
  async delCart(obj) {
    return await axios.post('/deleteShiZhuang', obj)
  },
  // 12.改变是否被选择
  async changeCart(obj) {
    return await axios.put('/checkShiZhuang', obj)
  },
  // 13.提交订单
  async payCart(obj) {
    return await axios.put('/PayCart', obj)
  },
  // 14.获取我的皮肤
  async getMySky(id) {
    return await axios.get('/getMySky?id=' + id,headers)
  },
  // 15.模糊查询
  async getSearch(name) {
    return await axios.get('/search?name=' + name)
  },
  // 16.根据id获取用户信息
  async getUser(id) {
    return await axios.get('/userById?id=' + id)
  },
}
