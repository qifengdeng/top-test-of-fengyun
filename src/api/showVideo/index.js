import axios from 'axios'
const headers = {
  headers: {
    Authorization: localStorage.token
  }
}
export default {
  // 按照这种方式写调用api接口
  // 调用自己使用接口编写函数时把下面的text函数删去
  async getRecolist() {
    return await axios.get('/anchor')
  },
  async getCommentById(id) {
    return await axios.get('/commentById?id=' + id)
  },
  async register(data) {
    return await axios.post('/register', data)
  },
  async addComment(data) {
    return await axios.post('/addComment', data, headers)
  },
  async getLikeNum(id) {
    return await axios.get('/anchorById?id=' + id)
  },
  async addLikeNum(id) {
    return await axios.put('/addlike', id)
  },
  async sendCode(data) {
    return await axios.post('/sendcode', data)
  },
  async login(data) {
    return await axios.post('/login', data)
  }

}
