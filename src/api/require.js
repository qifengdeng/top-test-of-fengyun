import { Toast } from 'vant'
import axios from 'axios'
// 进度条
import NProgress from 'nprogress'

// 设置axios的根路径
axios.defaults.baseURL = 'http://10.41.152.18:8080'

// 请求拦截
axios.interceptors.request.use(config=>{
  // 开始转圈
  NProgress.start();
  return config;
})

// 响应拦截
axios.interceptors.response.use((res) => {
  // 停止转圈
  NProgress.done();
  return res
}, (err) => {
  // 弹框显示提示错误
  Toast(err)
  // Promise对象 reject 提示错误信息
  Promise.reject(err)
})

export default axios
