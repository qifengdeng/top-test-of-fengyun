import axios from 'axios'

export default {
  // 按照这种方式写调用api接口
  // 调用自己使用接口编写函数时把下面的text函数删去
  async text1(params) {
    // 把调用接口请求到的数据返回出去
    return await axios.get('/api/getlunbo', params)
  },
  async getNewsInfo(params) {
    return await axios.get(`/${params}`)
  },
  async getnewlist(mokuai, id) {
    return await axios.get(`/getnews?mokuai=${mokuai}&nid=` + id)
  },
  async getalllist(params) {
    return await axios.get('/getallnews?mokuai=' + params)
  },
  async getNewsTitle(params) {
    return await axios.get('/getnewtitle', params)
  }
}
