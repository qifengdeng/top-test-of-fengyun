import axios from 'axios'
export default {
  async text(params) {
    // 把调用接口请求到的数据返回出去
    return await axios.get('/shizhuang', params)
  },
  async getZhuti(params) {
    return await axios.get('/getZhuti', params)
  }
}
