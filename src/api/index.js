import './require'
import Vue from 'vue'
import door from './door'
import home from './home'
import imgVideo from './imgVideo'
import news from './news'
import shopping from './shopping'
import showVideo from './showVideo'


Vue.prototype.$http = {
  ...door,
  ...home,
  ...imgVideo,
  ...news,
  ...shopping,
  ...showVideo

}
