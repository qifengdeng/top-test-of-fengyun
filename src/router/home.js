import Home from '../views/Home/Home.vue'
export default [
  {
    path: '/home',
    component: Home,
    meta: {
      Top: false,
      Bottom: true
    }
  }
]
