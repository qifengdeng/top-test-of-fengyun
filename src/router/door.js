import Door from '../views/Door/Door.vue'
import Banben from '../views/Door/banben.vue'
import Juqing from '../views/Door/juqing.vue'
export default [
  {
    path: '/door',
    component: Door,
    meta: {
      Top: true,
      Bottom: true
    }
  },
  {
    path: '/door/banben',
    component: Banben
  },
  {
    path: '/door/juqing',
    component: Juqing
  }
]
