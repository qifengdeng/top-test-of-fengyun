import News from '../views/Home/News/News.vue'
import NewsList from '../views/Home/News/News/NewsList'
import NewsInfo from '../views/Home/News/News/NewsInfo'
import NoticeInfo from '../views/Home/News/Notice/NoticeInfo'
import NoticeList from '../views/Home/News/Notice/NoticeList'
import ActiveInfo from '../views/Home/News/Active/ActiveInfo'
import ActiveList from '../views/Home/News/Active/ActiveList'
import MediaInfo from '../views/Home/News/Media/MediaInfo'
import MediaList from '../views/Home/News/Media/MediaList'

export default [
  {
    path: '/home/news',
    component: News
  },
  {
    path: '/home/news/newslist',
    component: NewsList,
    meta: {
      title: '新闻',
      isShow: true
    }
  },
  {
    path: '/home/news/newsinfo/:id',
    component: NewsInfo,
    props: true,
    meta: {
      nav: 'newsinfo',
      isShow: true
    }
  },
  {
    path: '/home/news/noticeinfo/:id',
    component: NoticeInfo,
    props: true,
    meta: {
      nav: 'noticeinfo',
      isShow: true
    }
  },
  {
    path: '/home/news/noticelist',
    component: NoticeList,
    meta: {
      title: '公告',
      isShow: true
    }
  },
  {
    path: '/home/news/activeinfo/:id',
    component: ActiveInfo,
    props: true,
    meta: {
      nav: 'activeinfo',
      isShow: true
    }
  },
  {
    path: '/home/news/activelist',
    component: ActiveList,
    meta: {
      title: '活动',
      isShow: true
    }
  },
  {
    path: '/home/news/mediainfo/:id',
    component: MediaInfo,
    props: true,
    meta: {
      nav: 'mediainfo',
      isShow: true
    }
  },
  {
    path: '/home/news/medialist',
    component: MediaList,
    meta: {
      title: '媒体',
      isShow: true
    }
  }
]
