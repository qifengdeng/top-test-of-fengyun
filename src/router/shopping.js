import Shopping from '../views/Home/Shopping/Shopping.vue'
import User from '../views/Home/Shopping/user/user.vue'
import ShoppingInfo from '../views/Home/Shopping/shoppinginfo.vue'
import Order from '../views/Home/Shopping/user/order.vue'
import MySky from '../views/Home/Shopping/user/mysky.vue'
export default [
  {
    path: '/home/shopping',
    component: Shopping
  },
  {
    path: '/home/shopping/user',
    component: User
  },
  {
    path: '/home/shopping/shoppinginfo/:id',
    component: ShoppingInfo,
    props:true
  },
  {
    path: '/home/shopping/order',
    component: Order
  },
  {
    path: '/home/shopping/user/mysky',
    component: MySky
  }
]
