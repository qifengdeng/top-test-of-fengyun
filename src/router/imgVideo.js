import ImgVideo from '../views/Home/ImgVideo/ImgVideo.vue'
export default [
  {
    path: '/home/imgvideo',
    component: ImgVideo
  },
  {
    path: '/home/imgvideo/wallpaper',
    component: () => import('../views/Home/ImgVideo/wallpaper')
  },
  {
    path: '/home/imgvideo/gamevideo',
    component: () => import('../views/Home/ImgVideo/gamevideo')
  },
  {
    path: '/home/imgvideo/music',
    component: () => import('../views/Home/ImgVideo/music')
  }
]
