import ShowVideo from '../views/Home/ShowVideo/ShowVideo.vue'
import Login from '../views/Home/ShowVideo/Login.vue'
import Register from '../views/Home/ShowVideo/Register.vue'
export default [
  {
    path: '/home/showvideo',
    component: ShowVideo
  },
  {
    path: '/home/login',
    component: Login
  },
  {
    path: '/home/register',
    component: Register
  }
]
