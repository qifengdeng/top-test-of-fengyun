import Vue from 'vue'
import VueRouter from 'vue-router'
import door from './door'
import home from './home'
import imgVideo from './imgVideo'
import shopping from './shopping'
import showVideo from './showVideo'
import news from './news'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/door'
  },
  ...door,
  ...home,
  ...imgVideo,
  ...showVideo,
  ...shopping,
  ...news
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
