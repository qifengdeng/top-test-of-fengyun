// 组件里都可以写生命周期
// 这样写 每个组件引入的时候都要写一次
// export default {
//   created() {
//     console.log("混入");
//   },
//   methods: {
//     goDetail(url, id) {
//       this.$router.push(url + "/" + id);
//     }
//   }
// }

// 所以写成全局配置 这样不用每个组件都引入一次
import Vue from 'vue'
Vue.mixin({
  methods: {
    goDetail(url, id) {
      this.$router.push(url + '/' + id)
    }
  }
})
