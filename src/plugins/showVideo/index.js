import Vue from 'vue'
import { Field, Form, Image, Lazyload, Grid, Button, Tab, Tabs, Icon, Search } from 'vant'
const options = {
  loading: '/showVideo/loading.gif'
}
Vue.use(Tab)
  .use(Tabs)
  .use(Button)
  .use(Icon)
  .use(Search)
  .use(Grid)
  .use(Lazyload, options)
  .use(Image)
  .use(Form)
  .use(Field)

