import Vue from 'vue'
import { Tab, List, Cell, Tabs, Pagination, Icon } from 'vant'

Vue.use(Tab)
Vue.use(Tabs)
Vue.use(List)
Vue.use(Cell)
Vue.use(Pagination)
Vue.use(Icon)
