import Vue from 'vue'
import { Tabbar, TabbarItem } from 'vant';
import { Icon } from 'vant';
import { NavBar } from 'vant';
import { Tab, Tabs } from 'vant';
import { Panel } from 'vant';
import { SubmitBar } from 'vant';
import { Card } from 'vant';
import { Switch } from 'vant';
import { Swipe, SwipeItem } from 'vant';
import { Dialog } from 'vant';
import { Lazyload } from 'vant';
import { Search } from 'vant';

// const options={
//   loading:'/shopping/lanlan/cbg/cbg_098.gif'
// }
import { Button } from 'vant';
import { Toast } from 'vant';


Vue.use(Button);
Vue.use(Search);
Vue.use(Lazyload, {
  lazyComponent: true,
  loading:'../../../public/shopping/lanlan/cbg/cbg_098.gif'
});
// 全局注册
Vue.use(Toast);
Vue.use(Dialog);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Switch);
Vue.use(Card);
Vue.use(SubmitBar);
Vue.use(Panel);
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(NavBar);
Vue.use(Icon);
Vue.use(Tabbar);
Vue.use(TabbarItem);