import Vue from 'vue'
import { Button, Tab, Tabs, Icon, Swipe, SwipeItem, Overlay } from 'vant'

Vue.use(Overlay)
Vue.use(Swipe)
Vue.use(SwipeItem)
Vue.use(Icon)
Vue.use(Tab)
Vue.use(Tabs)
Vue.use(Button)
